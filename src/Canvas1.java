// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// Canvas1.java メイン処理部
//
// (C)Tomarky   2009.01.01-
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

//インポート
import javax.microedition.lcdui.game.*;
import javax.microedition.lcdui.*;
import javax.microedition.midlet.*;
import java.util.*;                  //乱数や時間
import java.io.*;                  //InputStream OutputStream
//import javax.microedition.rms.*;   //データセーブ用
//import javax.microedition.media.*; //音楽再生
//import javax.microedition.media.control.*; //トーン・ボリューム
//import javax.microedition.io.*;      //通信


// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

//メイン処理(キャンバス)
public class Canvas1 extends GameCanvas implements CommandListener {
	private final int         DISPLAY_WIDTH  = 240,
	                          DISPLAY_HEIGHT = 268;
	private final int         CLEAR=-8, SEND=-10, SOFT1=-6, SOFT2=-7, SOFT3=-20, SOFT4=-21;
	private final long        SLEEPTIME = 100L;
	private MIDlet            midlet;
	private Graphics          g;
//	private boolean           RunningFlag=true;
	private boolean           CallFlag=false;
//	private int               keyEvent=-999;           //キーイベント
//	private int               keyPsEvent=-999;         //キープレスイベント
//	private int               keyRpEvent=-999;         //キーリピートイベント
//	private int               keyRsEvent=-999;         //キーリリースイベント
//	private int               keyState;              //キー状態
	private Command[]         commands=new Command[5];//コマンド
//	private int               cmdnum=-1;
//	private Image[]           images=new Image[3];

	private Font              smallFont=null,
	                          mediumFont=null, mediumULFont=null,
	                          largeFont=null;

	private long              longTotal=0L, 
	                          longAverage=0L,
	                          longNowInput=0L;
	private int               numCount=0, listIndex=0, selectIndex=0,
	                          menuselect=0, menutype=0, nextindex=0;
	
	private long[]            longData=new long[1000], longDouble=new long[1000];

	private boolean           inputMode=true, longMode=true, viewMode=true, 
	                          kakunin=false, menuMode=false;

	private long              longPoint=0L, longMaxPoint=1L;
	private double            dblTotal=0.0, dblAverage=0.0;//, dblTotalSm=0.0, dblTotalLg=0.0;
	private double[]          dblData=new double[1000];
	
	private Form              form      = null;
	private TextField         textfield = null;

	private String            version   = null;

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//コンストラクタ
	Canvas1(MIDlet m) {
		//キーイベントの抑制
		super(false);
		
		//MIDlet操作用
		midlet=m;
		version="ver"+midlet.getAppProperty("MIDlet-Version");
		
		smallFont=Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_SMALL);
		mediumFont=Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_MEDIUM);
		largeFont=Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_LARGE);
		mediumULFont=Font.getFont(Font.FACE_SYSTEM,Font.STYLE_UNDERLINED,Font.SIZE_MEDIUM);

		g=getGraphics();
		
		//コマンドの生成
		//commands[0]=new Command("はい",  Command.OK,    0);
		//commands[1]=new Command("いいえ",Command.CANCEL,0);
		commands[0]=new Command("終了",  Command.SCREEN,0);
		commands[1]=new Command("ﾒﾆｭｰ",  Command.SCREEN,1);
		commands[2]=new Command("戻る",  Command.SCREEN,0);
		commands[3]=new Command("実行",  Command.SCREEN,1);
		commands[4]=new Command("次へ",  Command.SCREEN,1);

		addCommand(commands[0]);
		addCommand(commands[1]);

		//コマンドリスナーの指定
		setCommandListener(this);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private String dividePlace(long num) {
		if ((num>=1000L)||(num<=-1000L)) {
			String sgn=num<0?"-":"";
			long   num1=Math.abs(num);
			String str1=Long.toString(num1),str2="";
			while (num1>1000L) {
				str2=","+str1.substring(str1.length()-3)+str2;
				num1/=1000L;
				str1=Long.toString(num1);
			}
			return sgn+str1+str2;
		} else {
			return Long.toString(num);
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //


	private String strDouble(long lngNum, long Pt, boolean bb) {
		if (Pt>1L) {
			long tempLong=Math.abs(lngNum);
			String str1=Long.toString(tempLong%Pt+Pt);
			return (lngNum<0L?"-":"")+Long.toString(tempLong/Pt)+"."+str1.substring(1);
		} else {
			return Long.toString(lngNum)+(bb?".0":(Pt>0L?".":""));
		}
	}
	private String strDouble(double dblNum) {
		String str1=Double.toString(Math.abs(dblNum));
		int n=str1.indexOf('E');
		if (n<0) {
			return Double.toString(dblNum);
		} else {
			String str2=str1.substring(0,1)+str1.substring(2,n);
			int d=Integer.parseInt(str1.substring(n+1));
			int m=str2.length()-1;
			String str3;
			if (d<0) {
				if (m==1) {
					if ((str2.substring(1)).equals("0")) {
						str2=str2.substring(0,1);
					}
				}
				str3="0.";
				for (int i=1;i<-d;i++) {
					str3+="0";
				}
				return (dblNum<0.0?"-":"")+str3+str2;
			} else {
				if (m<=d) {
					for (int i=0;i<d-m;i++) {
						str2+="0";
					}
					return (dblNum<0.0?"-":"")+str2+".0";
				} else  {
					str3=str2.substring(0,d+1)+"."+str2.substring(d+1);
					return (dblNum<0.0?"-":"")+str3;
				}
			}
		}
//		return "?";
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	public void refresh() {
		int ancTR=Graphics.TOP|Graphics.RIGHT,
		    ancTL=Graphics.TOP|Graphics.LEFT,
		    ancTH=Graphics.TOP|Graphics.HCENTER;
	
		g.setColor(0,0,0);
		g.fillRect(0,0,240,268);
		
		g.setColor(inputMode?0x00FFFF00:0x00FFFFFF);
		g.drawRect(5,62,230,30);

		g.setColor(255,255,255);
		
		g.setFont(smallFont);
		g.drawString(version,65,4,ancTL);
		
		g.drawString("合計 ",50,30,ancTR);
		g.drawString("平均 ",50,45,ancTR);
		if (longMode) {
			g.drawString(dividePlace(longTotal),210,30,ancTR);
			g.drawString(dividePlace(longAverage),210,45,ancTR);
		} else {
			if (viewMode) {
//				if ((longTotal==0L)&&(dblTotal!=0.0)) {
					g.drawString(strDouble(dblTotal),210,30,ancTR);
//				} else {
//					g.drawString(strDouble(longTotal,longMaxPoint,true),210,30,ancTR);
//				}
				g.drawString(strDouble(dblAverage),210,45,ancTR);
			} else {
				//g.drawString(strDouble(dblTotal),210,30,ancTR);
				g.drawString(Double.toString(dblTotal),210,30,ancTR);
				g.drawString(Double.toString(dblAverage),210,45,ancTR);
			}
		}
		g.drawString("Count "+Integer.toString(numCount),5,93,ancTL);
		if ((!longMode)&&(!viewMode)) {
			g.drawString("指",240,93,ancTR);
		}
		
		g.setFont(largeFont);
		if (longMode) {
			g.drawString(dividePlace(longNowInput),210,67,ancTR);
		} else {
			g.drawString(strDouble(longNowInput,longPoint,false),210,67,ancTR);
//			if (longPoint>1L) {
//				long tempLong=Math.abs(longNowInput);
//				String str1=Long.toString(tempLong%longPoint+longPoint);
//				g.drawString((longNowInput<0L?"-":"")+Long.toString(tempLong/longPoint)+"."
//				             +str1.substring(1)
//				                 ,210,67,ancTR);
//			} else {
//				g.drawString(Long.toString(longNowInput)+(longPoint>0L?".":"")
//				                 ,210,67,ancTR);
//			}
		}

		g.setFont(mediumFont);
		g.drawString("足算電卓",0,0,ancTL);
		g.drawString(longMode?"整数":"実数",240,0,ancTR);
		
		int n;
		for (int i=0;(i<7)&&(i<numCount);i++) {
			n=listIndex-i;
			if ((!inputMode)&&(n==selectIndex)) {
				g.setFont(mediumULFont);
				g.setColor(255,255,0);
			}
			g.drawString(Integer.toString(n+1),50,i*20+112,ancTR);
			if (longMode) {
				g.drawString(dividePlace(longData[n]),210,i*20+112,ancTR);
			} else {
				if (viewMode) {
					g.drawString(strDouble(longDouble[n],longData[n],true),210,i*20+112,ancTR);
				} else {
					g.drawString(Double.toString(dblData[n]),210,i*20+112,ancTR);
				}
			}
			if ((!inputMode)&&(n==selectIndex)) {
				g.setFont(mediumFont);
				g.setColor(255,255,255);
			}
		}
		
		int dw=80, dy=70, hc=DISPLAY_WIDTH/2;
		int dx=(DISPLAY_WIDTH-dw)/2;
		
		if ((menuMode)&&(!kakunin)) {
			g.setColor(255,255,255);
			g.fillRect(dx,dy,dw,20);
			g.setColor(menuselect==0?0x00FFFF00:0x007F7F7F);
			g.fillRect(dx,dy+20,dw,20);
			g.setColor(menuselect==1?0x00FFFF00:0x007F7F7F);
			g.fillRect(dx,dy+40,dw,20);
			g.setColor(menuselect==2?0x00FFFF00:0x007F7F7F);
			g.fillRect(dx,dy+60,dw,20);
			g.setColor(menuselect==3?0x00FFFF00:0x007F7F7F);
			g.fillRect(dx,dy+80,dw,20);
			g.setColor(menuselect==4?0x00FFFF00:0x007F7F7F);
			g.fillRect(dx,dy+100,dw,20);
			
			g.setColor(255,255,255);
			g.drawRect(dx,dy,dw,120);
			g.drawRect(dx,dy+20,dw,20);
			g.drawRect(dx,dy+60,dw,40);
			g.drawLine(dx,dy+80,dx+dw-1,dy+80);

			g.setColor(0,0,0);
			g.setFont(smallFont);
			g.drawString("メニュー",hc,dy+4,ancTH);
			g.setColor(menuselect==0?0x00FF0000:0x00000000);
			g.drawString("リスト",hc,dy+24,ancTH);
			g.setColor(menuselect==1?0x00FF0000:0x00000000);
			g.drawString("結果表示",hc,dy+44,ancTH);
			g.setColor(menuselect==2?0x00FF0000:0x00000000);
			g.drawString("一括入力",hc,dy+64,ancTH);
			g.setColor(menuselect==3?0x00FF0000:0x00000000);
			g.drawString(longMode?"実数モード":"整数モード",hc,dy+84,ancTH);
			g.setColor(menuselect==4?0x00FF0000:0x00000000);
			g.drawString("全削除",hc,dy+104,ancTH);
		}
		
		if (kakunin) {
			String[] mes={"削除します","切り替えます","終了します","全削除します"};
			
			g.setColor(255,255,255);
			g.fillRect(80,90,80,20);
			g.setColor(menuselect==0?0x00FFFF00:0x007F7F7F);
			g.fillRect(80,110,80,20);
			g.setColor(menuselect==1?0x00FFFF00:0x007F7F7F);
			g.fillRect(80,130,80,20);
			
			g.setColor(255,255,255);
			g.drawRect(80,90,80,60);
			g.drawRect(80,110,80,20);
			
			g.setColor(0,0,0);
			g.setFont(smallFont);
			g.drawString(mes[menutype],120,94,ancTH);
			g.setColor(menuselect==0?0x00FF0000:0x00000000);
			g.drawString("はい",120,114,ancTH);
			g.setColor(menuselect==1?0x00FF0000:0x00000000);
			g.drawString("いいえ",120,134,ancTH);
		}
		
		flushGraphics();
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void inputNumber(int num) {
		inputNumber(num,true);
	}
	private void inputNumber(int num, boolean doRefresh) {
		inputMode=true;
		long testNum=longNowInput*10L;
		if (testNum>10000000000000L) {
			return;
		}
		if (longPoint>10000000000000L) {
			return;
		}
		
		longNowInput=testNum+(long)num*(testNum<0?-1L:1L);
		if ((longMode==false)&&(longPoint>0)) {
			longPoint*=10L;
		}
		if (doRefresh) {
			refresh();
		}
	}
	
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void clearNumber() {
		if (longMode) {
			longNowInput/=10L;
		} else {
			if (longPoint==1L) {
				longPoint=0L;
			} else {
				longNowInput/=10L;
				longPoint/=10L;
			}
		}
		refresh();
	}
	
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private double ceil(double v) {
		double a=(double)longMaxPoint;
		return Math.floor(v*a+0.5)/a;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	
	private void addNumber() {
		addNumber(true);
	}
	private void addNumber(boolean doRefresh) {
		if (numCount>=1000L) return;
		inputMode=true;
		selectIndex=listIndex=numCount;
		numCount++;
		if (longMode) {
			longData[listIndex]=longNowInput;
			longTotal+=longNowInput;
			longAverage=longTotal/(long)numCount;
		} else {
			dblData[listIndex]=(double)longNowInput;
			longData[listIndex]=longPoint;
			longDouble[listIndex]=longNowInput;
			if (longPoint>1L) {
				dblData[listIndex]/=(double)longPoint;
			}
//			if (Math.abs(dblData[listIndex])<1.0) {
//				dblTotalSm+=dblData[listIndex];
//			} else {
//				dblTotalLg+=dblData[listIndex];
//			}
//			dblTotal=dblTotalLg+dblTotalSm;
			dblTotal+=dblData[listIndex];

			longMaxPoint=Math.max(longMaxPoint,longPoint);
			dblTotal=ceil(dblTotal);
			//★★★
//			double tmpDbl=dblTotal*(double)longMaxPoint;
//			if ((tmpDbl>(double)Long.MAX_VALUE)||(tmpDbl<(double)Long.MIN_VALUE)) {
//				longTotal=0L;
//			} else {
//				longTotal=(long)tmpDbl; //ここオーバーフローおこす
//			}
			//★★★　もう１箇所ある
			dblAverage=dblTotal/(double)numCount;
			longPoint=0L;
		}
		longNowInput=0L;
		if (doRefresh) {
			refresh();
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//キープレスイベント
	public void keyPressed(int keyCode) {
		if (keyCode==0) return;
		
		if (kakunin) {
			keyPressedKakunin(keyCode);
			return;
		}
		
		if (menuMode) {
			keyPressedMenuMode(keyCode);
			return;
		}
		
		switch (keyCode) {
		case SEND:
//			if (numCount>0) {
//				menuselect=1;
//				menutype=1;
//				kakunin=true;
//			} else {
//				changeMode();
//			}
			if (!longMode) {
				viewMode=!viewMode;
				refresh();
			}
			break;
		case KEY_NUM0: inputNumber(0); break;
		case KEY_NUM1: inputNumber(1); break;
		case KEY_NUM2: inputNumber(2); break;
		case KEY_NUM3: inputNumber(3); break;
		case KEY_NUM4: inputNumber(4); break;
		case KEY_NUM5: inputNumber(5); break;
		case KEY_NUM6: inputNumber(6); break;
		case KEY_NUM7: inputNumber(7); break;
		case KEY_NUM8: inputNumber(8); break;
		case KEY_NUM9: inputNumber(9); break;
		case CLEAR:
			if (inputMode) {
				clearNumber();
			} else if (numCount>0) {
				menuselect=1;
				menutype=0;
				kakunin=true;
				refresh();
			}
			break;
		case KEY_POUND:
			longNowInput=-longNowInput;
			refresh();
			break;
		case KEY_STAR:
			if ((longMode==false)&&(longPoint==0L)) {
				longPoint=1L;
				refresh();
			}
			break;
		default:
			switch (getGameAction(keyCode)) {
			case RIGHT:
				if ((numCount>0)&&(selectIndex>=0)&&(selectIndex<numCount)) {
					if (longMode) {
						longNowInput=longData[selectIndex];
					} else {
						double tmp=dblData[selectIndex];
						if (longData[selectIndex]>0L) {
							tmp*=(double)longData[selectIndex];
						}
						//longNowInput=(long)tmp;
						longNowInput=longDouble[selectIndex];
						longPoint=longData[selectIndex];
					}
					if (inputMode==false) {
						inputMode=true;
					}
					if (selectIndex<(numCount-1)) {
						selectIndex++;
					}
					refresh();
				}
				break;
			case LEFT:
				if ((numCount>0)&&(selectIndex>=0)&&(selectIndex<numCount)) {
					if (longMode) {
						longNowInput=longData[selectIndex];
					} else {
						double tmp=dblData[selectIndex];
						if (longData[selectIndex]>0L) {
							tmp*=(double)longData[selectIndex];
						}
//						longNowInput=(long)tmp;
						longNowInput=longDouble[selectIndex];
						longPoint=longData[selectIndex];
					}
					if (selectIndex>0) {
						selectIndex--;
					}
					if (inputMode==false) {
						inputMode=true;
					}
					refresh();
				}
				break;
			case UP:
				if (inputMode==false) {
					if (selectIndex<numCount-1) {
						selectIndex++;
						if (selectIndex>listIndex) {
							listIndex++;
						}
					} else {
						inputMode=true;
					}
					refresh();
				}
				break;
			case DOWN:
				if (inputMode) {
					inputMode=false;
					selectIndex=listIndex;
				} else {
					if (selectIndex>0) {
						selectIndex--;
						if (selectIndex<listIndex-6) {
							listIndex--;
						}
					}
				}
				refresh();
				break;
			case FIRE:
				if (inputMode) {
					addNumber();
				} else {
					inputMode=true;
					refresh();
				}
				break;
			}
			break;
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

//*
	//キーリピートイベント
	public void keyRepeated(int keyCode) {
		if (keyCode==0) return;
		
		if (kakunin) return;
		if (menuMode) return;
		
		switch (keyCode) {
		case KEY_NUM2:
		case KEY_NUM8:
			break;
		case CLEAR:
			if ((longNowInput!=0L)||(longPoint!=0L)) {
				longNowInput=0L;
				longPoint=0L;
				refresh();
			}
			break;
		default:
			switch (getGameAction(keyCode)) {
			case UP:
				if (!inputMode) {
					if (selectIndex<numCount-1) {
						selectIndex++;
						if (selectIndex>listIndex) {
							listIndex++;
						}
					} else {
						inputMode=true;
					}
					refresh();
				}
				break;
			case DOWN:
				if (!inputMode) {
					if (selectIndex>0) {
						selectIndex--;
						if (selectIndex<listIndex-6) {
							listIndex--;
						}
					}
				}
				refresh();
				break;
			}
			break;
		}
	}
//*/

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

/*
	//キーリリースイベント
	public void keyReleased(int keyCode) {
	}
//*/

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//コマンドイベント
	public void commandAction(Command c,Displayable disp) {
		if (kakunin) return;
		if (c==commands[0]) {
			if (numCount>0) {
				menutype=2;
				menuselect=1;
				kakunin=true;
				refresh();
			} else {
				midlet.notifyDestroyed();
			}
		} else if (c==commands[1]) {
			menuMode=!menuMode;
			if (menuMode) {
				menuselect=1;
			}
			refresh();
		} else if (c==commands[2]) {
			(Display.getDisplay(midlet)).setCurrent(this);
		} else if (c==commands[3]) {
			char[] text=(textfield.getString()).toCharArray();
			int flag=0,sgn=0;
			longNowInput=0L;
			longPoint=0L;
			for (int i=0;i<text.length;i++) {
				switch (text[i]) {
				case '0': case '０': if (flag<2) { inputNumber(0,false); flag=1;} break;
				case '1': case '１': if (flag<2) { inputNumber(1,false); flag=1;} break;
				case '2': case '２': if (flag<2) { inputNumber(2,false); flag=1;} break;
				case '3': case '３': if (flag<2) { inputNumber(3,false); flag=1;} break;
				case '4': case '４': if (flag<2) { inputNumber(4,false); flag=1;} break;
				case '5': case '５': if (flag<2) { inputNumber(5,false); flag=1;} break;
				case '6': case '６': if (flag<2) { inputNumber(6,false); flag=1;} break;
				case '7': case '７': if (flag<2) { inputNumber(7,false); flag=1;} break;
				case '8': case '８': if (flag<2) { inputNumber(8,false); flag=1;} break;
				case '9': case '９': if (flag<2) { inputNumber(9,false); flag=1;} break;
				case '.': case '．':
					if (longMode) {
						if (flag==1) {
							flag=2;
						} else if (flag!=2) {
							flag=3;
						}
					} else {
						if (longPoint==0L) {
							longPoint=1L;
						} else {
							if (flag==1) {
								flag=2;
							} else if (flag!=2) {
								flag=3;
							}
						}
					}
					break;
				case '-': case '−':
					switch (flag) {
					case 0:  sgn=1;  break;
					case 1:  flag=2; break;
					default: flag=3; break;
					}
					break;
				default:
					if ((flag>0)&&(flag<3)) {
						if (sgn>0) {
							longNowInput=-longNowInput;
						}
						addNumber(false);
					}
					flag=0; sgn=0;
					break;
				}
			}
			if ((flag>0)&&(flag<3)) {
				if (sgn>0) {
					longNowInput=-longNowInput;
				}
				addNumber(false);
			}
			menuMode=false;
			refresh();
			(Display.getDisplay(midlet)).setCurrent(this);
		} else if (c==commands[4]) {
			form.deleteAll();
			ByteArrayOutputStream baos=new ByteArrayOutputStream(1);
			PrintStream ps=new PrintStream(baos);
			textfield.setString(null);
			int startindex=nextindex;
			if (longMode) {
				for (int i=startindex;i<numCount;i++) {
					ps.print(i+1); ps.println(": "+dividePlace(longData[i]));
					if (baos.size()>1950) {
						nextindex=i+1;
						break;
					}
				}
			} else {
				for (int i=startindex;i<numCount;i++) {
					ps.print(i+1); ps.print(": "); ps.println(dblData[i]);
					if (baos.size()>1950) {
						nextindex=i+1;
						break;
					}
				}
			}
			textfield.setString(baos.toString());
			form.append(textfield);
			ps.close();
			ps=null; baos=null;
			if ((nextindex>=numCount)||(startindex==nextindex)) {
				form.removeCommand(commands[4]);
			}
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

/*
	//コメント描写
	public void drawComment(String s) {
		g.setColor(255,255,255);
		g.fillRect(0,DISPLAY_HEIGHT-20,DISPLAY_WIDTH,20);
		g.setColor(0,128,255);
		g.drawRect(0,DISPLAY_HEIGHT-20,DISPLAY_WIDTH,20);
		g.setColor(0,0,0);
		g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_MEDIUM));
		g.drawString(s,DISPLAY_WIDTH/2,DISPLAY_HEIGHT-18,Graphics.HCENTER|Graphics.TOP);
	}
//*/

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

/*
	//中抜き文字の描写
	private void drawNukiMoji(String s, int x, int y, int rIn, int gIn, int bIn, int anchor) {
		int i,j;
		
		for (i=-1;i<2;i++)
			for (j=-1;j<2;j++)
				g.drawString(s,x+j,y+i,anchor);
		g.setColor(rIn,gIn,bIn);
		g.drawString(s,x,y,anchor);
	}
	private void drawNukiMoji(String s, int x, int y, int rIn,   int gIn,  int bIn,
	                                                  int rOut,  int gOut, int bOut, int anchor) {
		g.setColor(rOut,gOut,bOut);
		drawNukiMoji(s,x,y,rIn,gIn,bIn,anchor);
	}
//*/

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

/*
	private Random            rndm=new Random();      //乱数
	//乱数の計算
	private int rand(int num) {
		return (rndm.nextInt()>>>1)%num;
	} 
//*/

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//終了命令
	public void callFinish() {
//		RunningFlag=false;
		CallFlag=true;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void changeMode() {
		numCount=0; listIndex=0; selectIndex=0;
		longNowInput=0L;
		longTotal=0L; longAverage=0L;
		longPoint=0L; longMaxPoint=1L;
		dblTotal=0.0; dblAverage=0.0;
//		dblTotalSm=0.0; dblTotalLg=0.0;
		longMode=!longMode;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void keyPressedKakunin(int keyCode) {
		switch (keyCode) {
		case CLEAR:
			kakunin=false;
			refresh();
			break;
		default:
			switch (getGameAction(keyCode)) {
			case UP:
			case DOWN:
				menuselect=1-menuselect;
				refresh();
				break;
			case FIRE:
				if (menuselect==0) {
					switch (menutype) {
					case 3:
						longMode=!longMode;
						changeMode();
						break;
					case 2:
						midlet.notifyDestroyed();
						return;
					case 1:
						changeMode();
						break;
					case 0:
						numCount--;
						if (longMode) {
							longTotal-=longData[selectIndex];
							if (numCount>0) {
								longAverage=longTotal/(long)numCount;
							} else {
								longAverage=0L;
							}
							for (int i=selectIndex;i<numCount;i++) {
								longData[i]=longData[i+1];
							}
						} else {
							for (int i=selectIndex;i<numCount;i++) {
								dblData[i]=dblData[i+1];
								longData[i]=longData[i+1];
								longDouble[i]=longDouble[i+1];
							}
							long temp=1L;
							dblTotal=0.0;// dblTotalSm=0.0; dblTotalLg=0.0;
							for (int i=0;i<numCount;i++) {
//								if (dblData[i]<1.0) {
//									dblTotalSm+=dblData[i];
//								} else {
//									dblTotalLg+=dblData[i];
//								}
								dblTotal+=dblData[i];
								temp=Math.max(temp,longData[i]);
							}
//							dblTotal=dblTotalLg+dblTotalSm;
							longMaxPoint=temp;
							dblTotal=ceil(dblTotal);
							//★★★
///							double tmpDbl=dblTotal*(double)longMaxPoint;
//							longTotal=(long)tmpDbl; //ここオーバーフロー起こす
							//★★★
							if (numCount>0) {
								dblAverage=dblTotal/(double)numCount;
							} else {
								dblAverage=0.0;
								longMaxPoint=1L;
							}
						}
						if (listIndex==numCount) {
							listIndex--;
						}
						if (selectIndex==numCount) {
							selectIndex--;
						}
						break;
					}
				}
				kakunin=false;
				refresh();
				break;
			}
			break;
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void keyPressedMenuMode(int keyCode) {
		int gamekey=-999;
		switch (keyCode) {
		case KEY_NUM1:
			if (menuselect!=0) { menuselect=0; refresh(); return; }
			gamekey=FIRE; break;
		case KEY_NUM2:
			if (menuselect!=1) { menuselect=1; refresh(); return; }
			gamekey=FIRE; break;
		case KEY_NUM3:
			if (menuselect!=2) { menuselect=2; refresh(); return; }
			gamekey=FIRE; break;
		case KEY_NUM4:
			if (menuselect!=3) { menuselect=3; refresh(); return; }
			gamekey=FIRE; break;
		case KEY_NUM5:
			if (menuselect!=4) { menuselect=4; refresh(); return; }
			gamekey=FIRE; break;
		case KEY_NUM8:
			return;
		case CLEAR:
			menuMode=false;
			refresh();
			return;
		default:
			gamekey=getGameAction(keyCode);
			break;
		}

		switch (gamekey) {
		case UP:
			menuselect=(menuselect+4)%5;
			refresh();
			break;
		case DOWN:
			menuselect=(menuselect+1)%5;
			refresh();
			break;
		case FIRE:
			if (menuselect<3) {
				if (form==null) {
					form=new Form(null);
					textfield=new TextField(null,null,2000,TextField.ANY);
					form.append(textfield);
					form.addCommand(commands[2]);
					form.setCommandListener(this);
				}
				ByteArrayOutputStream baos = new ByteArrayOutputStream(1);
				PrintStream           ps   = new PrintStream(baos);
				switch (menuselect) {
				case 0:
					form.setTitle("リスト");
					form.removeCommand(commands[3]);
					form.removeCommand(commands[4]);
					textfield.setLabel("Count "+Integer.toString(numCount));
					if (numCount>0) {
						if (longMode) {
							for (int i=0;i<numCount;i++) {
								ps.print(i+1); ps.println(": "+dividePlace(longData[i]));
								if (baos.size()>1950) {
									nextindex=i+1;
									if (nextindex<numCount) {
										form.addCommand(commands[4]);
									}
									break;
								}
							}
						} else {
							for (int i=0;i<numCount;i++) {
								ps.print(i+1);
								ps.print(": ");
								if (viewMode) {
									ps.println(strDouble(longDouble[i],longData[i],true));
								} else {
									ps.println(dblData[i]);
								}
								if (baos.size()>1950) {
									form.addCommand(commands[4]);
									if (nextindex<numCount) {
										form.addCommand(commands[4]);
									}
									break;
								}
							}
						}
					}
					textfield.setString(baos.toString());
					break;
				case 1:
					form.setTitle("結果表示");
					form.removeCommand(commands[3]);
					form.removeCommand(commands[4]);
					textfield.setLabel(null);
					ps.print("Count "); ps.println(numCount); ps.println();
					if (longMode) {
						if (longTotal>=1000L) {
							ps.println("桁区切りなし");
						}
						ps.print(" 合計 "); ps.println(longTotal);
						ps.print(" 平均 "); ps.println(longAverage);
						if (longTotal>=1000L) {
							ps.println(); ps.println();
							ps.println("桁区切りあり");
							ps.println(" 合計 "+dividePlace(longTotal));
							ps.println(" 平均 "+dividePlace(longAverage));
						}
					} else {
//						ps.print(" 合計 "); ps.println(strDouble(longTotal,longMaxPoint,true));
						ps.print(" 合計 "); ps.println(strDouble(dblTotal));
						ps.print(" 平均 "); ps.println(strDouble(dblAverage));
						int n=(Double.toString(dblTotal)).indexOf('E');
						int m=(Double.toString(dblAverage)).indexOf('E');
						if ((n>0)||(m>0)) {
							ps.println(); ps.println();
							ps.println("指数表記");
							ps.print(" 合計 "); ps.println(dblTotal);
							ps.print(" 平均 "); ps.println(dblAverage);
						}
						ps.println(); ps.println();
						ps.println("【注意】");
						ps.println(
			"浮動小数点数を用いた計算のため桁落ち・丸め誤差などにより計算結果が正確でないことがあります");
					}
					textfield.setString(baos.toString());
					break;
				case 2:
					if (longMode) {
						form.setTitle("一括入力");
					} else {
						form.setTitle("一括入力(指数表記不可)");
					}
					form.removeCommand(commands[4]);
					form.addCommand(commands[3]);
					textfield.setLabel(null);
					textfield.setString(null);
					break;
				}
				ps.close();
				ps=null; baos=null;
				(Display.getDisplay(midlet)).setCurrent(form);
				break;
			} else {
				menuMode=false;
				switch (menuselect) {
				case 3:
					if (numCount>0) {
						menuselect=1;
						menutype=1;
						kakunin=true;
					} else {
						changeMode();
					}
					break;
				case 4:
					if (numCount>0) {
						menuselect=1;
						menutype=3;
						kakunin=true;
					}
					break;
				}
				refresh();
			}
		}

	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

} //Canvas1の終わり

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
