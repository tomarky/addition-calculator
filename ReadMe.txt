オープンアプリ『足算電卓』

ひたすら数値を足していって
合計値と平均値を求められる
(足していった数値を確認できる)

アプリ配布リンク: http://tomarky.html.xdomain.jp/mobile/oap/p029.hdml


※オープンアプリとはau携帯電話(ガラケー)で動かすことができるアプリのことです
　(参考リンク http://www.au.kddi.com/ezfactory/tec/spec/openappli.html ）